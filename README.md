# Launches

### Backend

![Backend](https://i.imgur.com/ZYuLWs6.png)

The backend was developed in Typescript, with Node and using the NestJS framework. This choice was made for the fact that the framework is less opinionated, highly modular and promotes good practices, such as Domain-driven design.

The development was made using Test-driven development, utilizing the framework's watch mode for running tests as code is written. The architecture is event-driven (CQRS), and I sought to make it as clean and decoupled as possible, while respecting the SOLID principles.

An API Browser (Swagger) was added to facilitate the understanding and automated integration (by tools like swagger codegen, nswag and similar).

SpaceX integration was created through a wrapper module ([spacex-api.js](https://www.npmjs.com/package/spacex-api.js)).

The API consists of four main routes:

- **/launches/next:** returns the next launch
- **/launches/previous:** returns the latest launch
- **/launches/history:** retuns the 5 latest launches
- **/launches/schedule:** returns the 5 next launches

Instructions:

```bash
$ cd backend
$ yarn
$ yarn start:dev
```


### Frontend

![Frontend](https://i.imgur.com/DrpFqrH.png)

The frontend was created in Javascript with React and using the Redux framework. The choice was made because React has a vast amount of components e documentation, while Redux helps to keep the application's state under greater control.

The layout was created using the And Design UI kit and the integration with the backend was implemented using Axios. The frontend's architecture was created based on the boilerplate [create-react-app-redux](https://github.com/notrab/create-react-app-redux).

The application consists, basicaly of three containers (screens):

- **launches-summary:** home screen, shows cards with the next and latest launches
- **launches-history:** shows a timeline of the latest 5 launches
- **launches-schedule:** shows a timeline of the next 5 launches

It was also configured the integration of the frontend with HotJar though the module [react-hotjar](https://www.npmjs.com/package/react-hotjar). Google Optimize was also added through the JS tag provided by Google. Aditionaly [react-optimize](https://www.npmjs.com/package/react-optimize) was also configured. These tools were added so that one can better understand the user's behaviour and perform A/B tests on live code targeting a specific segment of users.

Instructions:

```bash
$ cd frontend
$ yarn
$ yarn start
```


### CI/CD

![CICD](https://i.imgur.com/2bBp7hT.png)

The CI/CD pipeline was created using GitLab and consists of three steps:

- **Test:** in this step, automated tests are executed and the code coverage is measured. The coverage report is published on [Gitlab Pages](https://lcn.andre.gitlab.io/desafio-rd/).
- **Deploy backend:** in this step, the backend is deployed to Heroku.
- **Deploy frontend:** in this step, the frontend is deployted to Heroku.

The pipeline is executed after each commit to the repository and the deployment steps are configured to be manual, requiring a user action on GitLab to publish the commit to production.
