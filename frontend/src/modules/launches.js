import { LaunchesService } from '../services/launchesService';

export const SUMMARY_LOADING = 'launches/SUMMARY_LOADING';
export const SET_NEXT_LAUNCH = 'launches/SET_NEXT_LAUNCH';
export const SET_PREVIOUS_LAUNCH = 'launches/SET_PREVIOUS_LAUNCH';
export const LAUNCH_HISTORY_LOADING = 'launches/LAUNCH_HISTORY_LOADING';
export const SET_LAUNCH_HISTORY = 'launches/SET_LAUNCH_HISTORY';
export const LAUNCH_SCHEDULE_LOADING = 'launches/LAUNCH_SCHEDULE_LOADING';
export const SET_LAUNCH_SCHEDULE = 'launches/SET_LAUNCH_SCHEDULE';

const initialState = {
  summaryLoading: true,
  launchHistoryLoading: true,
  launchScheduleLoading: true,
  nextLaunch: undefined,
  previousLaunch: undefined,
  launchHistory: [],
  launchSchedule: [],
}

export default (state = initialState, action) => {
  switch (action.type) {
    case SET_NEXT_LAUNCH:
      return {
        ...state,
        nextLaunch: action.nextLaunch,
      };

    case SET_PREVIOUS_LAUNCH:
      return {
        ...state,
        previousLaunch: action.previousLaunch,
      };

    case SET_LAUNCH_HISTORY:
      return {
        ...state,
        launchHistory: action.launchHistory,
      };
    
    case SET_LAUNCH_SCHEDULE:
      return {
        ...state,
        launchSchedule: action.launchSchedule,
      };
    
    case SUMMARY_LOADING:
      return {
        ...state,
        summaryLoading: action.summaryLoading,
      };
    
    case LAUNCH_HISTORY_LOADING:
      return {
        ...state,
        launchHistoryLoading: action.launchHistoryLoading,
      };
    
    case LAUNCH_SCHEDULE_LOADING:
      return {
        ...state,
        launchScheduleLoading: action.launchScheduleLoading,
      };

    default:
      return state;
  }
}

export const getSummary = () => {
  return async (dispatch) => {
    dispatch({
      type: SUMMARY_LOADING,
      summaryLoading: true,
    });

    try {
      dispatch({
        type: SET_NEXT_LAUNCH,
        nextLaunch: await LaunchesService.getNextLaunch(),
      });
      dispatch({
        type: SET_PREVIOUS_LAUNCH,
        previousLaunch: await LaunchesService.getPreviousLaunch(),
      });
    } finally {
      dispatch({
        type: SUMMARY_LOADING,
        summaryLoading: false,
      });
    }
  };
};

export const getLaunchHistory = () => {
  return async (dispatch) => {
    dispatch({
      type: LAUNCH_HISTORY_LOADING,
      launchHistoryLoading: true,
    });

    try {
      dispatch({
        type: SET_LAUNCH_HISTORY,
        launchHistory: await LaunchesService.getLaunchHistory(),
      });
    } finally {
      dispatch({
        type: LAUNCH_HISTORY_LOADING,
        launchHistoryLoading: false,
      });
    }
  };
};

export const getLaunchSchedule = () => {
  return async (dispatch) => {
    dispatch({
      type: LAUNCH_SCHEDULE_LOADING,
      launchScheduleLoading: true,
    });

    try {
      dispatch({
        type: SET_LAUNCH_SCHEDULE,
        launchSchedule: await LaunchesService.getLaunchSchedule(),
      });
    } finally {
      dispatch({
        type: LAUNCH_SCHEDULE_LOADING,
        launchScheduleLoading: false,
      });
    }
  };
};
