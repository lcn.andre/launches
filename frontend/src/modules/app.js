import { message } from 'antd';

export const ERROR_THROWN = 'app/ERROR_THROWN';

const initialState = {};

export default (state = initialState, action) => {
  switch (action.type) {
    case ERROR_THROWN:
      message.error(action.errorMessage);
      return state;

    default:
      return state;
  }
}
