import { combineReducers } from 'redux';

import app from './app';
import launches from './launches';

export default combineReducers({
  app,
  launches,
});
