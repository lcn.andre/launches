export declare class LaunchDto {
    flightName: string;
    fireDate: Date;
    details: string;
    
    constructor(data?: undefined | any);
}

export declare class LaunchesService {
    static getNextLaunch(): Promise<LaunchDto>;
    static getPreviousLaunch(): Promise<LaunchDto>;
    static getLaunchHistory(): Promise<LaunchDto[]>;
    static getLaunchSchedule(): Promise<LaunchDto[]>;
}
