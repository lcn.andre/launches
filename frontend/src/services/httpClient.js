import axios from 'axios';

import store from '../store';
import { ERROR_THROWN } from '../modules/app';

const baseURL = process.env.REACT_APP_APIURL;

const httpClient = axios.create({
  baseURL,
  headers: {
    'Content-Type': 'application/json',
  },
});

httpClient.interceptors.response.use(
  (response) => response,
  (error) => {
    let errorMessage = error.message;

    if (error.response && error.response.data && error.response.data.message)
      errorMessage = error.response.data.message;

    store.dispatch({
      type: ERROR_THROWN,
      erro: errorMessage,
    });
  }
);

export default httpClient;
