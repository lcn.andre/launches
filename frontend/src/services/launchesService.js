import httpClient from './httpClient';

const BASE_URL = `/launches`;

const formatFireDate = (dateStr) => (dateStr ? new Date(dateStr).toLocaleDateString() : '');

export class LaunchesService {
    static async getNextLaunch() {
        const response = await httpClient.get(`${BASE_URL}/next`);

        response.data.fireDate = formatFireDate(response.data.fireDate);

        return response.data;
    }

    static async getPreviousLaunch() {
        const response = await httpClient.get(`${BASE_URL}/previous`);

        response.data.fireDate = formatFireDate(response.data.fireDate);

        return response.data;
    }

    static async getLaunchHistory() {
        const response = await httpClient.get(`${BASE_URL}/history`);

        return response.data.map(d => {
            d.fireDate = formatFireDate(d.fireDate);
            return d;
        });
    }

    static async getLaunchSchedule() {
        const response = await httpClient.get(`${BASE_URL}/schedule`);

        return response.data.map(d => {
            d.fireDate = formatFireDate(d.fireDate);
            return d;
        });
    }
}
