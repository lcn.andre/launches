import React from 'react';
import { hotjar } from 'react-hotjar';
import { Route, Switch, Link, withRouter } from 'react-router-dom';
import { push } from 'connected-react-router';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Layout, Menu, Breadcrumb, Typography } from 'antd';
import { ProfileOutlined, CalendarOutlined, HistoryOutlined } from '@ant-design/icons';

import LaunchesSummary from '../launches/launches-summary';
import LaunchesHistory from '../launches/launches-history';
import LaunchesSchedule from '../launches/launches-schedule';

const { Title } = Typography;
const { Header, Content, Sider } = Layout;

class App extends React.Component {
  state = {
    nextLaunch: undefined,
  };

  componentDidMount() {
    hotjar.initialize('2483030', '6');
  }

  render() {
    const breadcrumbNameMap = {
      '/': 'Resumo',
      '/history': 'Lançamentos anteriores',
      '/schedule': 'Próximos lançamentos',
    };
    const menuNameMap = {
      '': 'summary',
      'history': 'launchesHistory',
      'schedule': 'nextLaunches',
    };
    const { location } = this.props;
    const pathSnippets = location.pathname.split('/').filter(i => i);
    if (!pathSnippets.length)
      pathSnippets.push('/');
    const extraBreadcrumbItems = pathSnippets.map((_, index) => {
      const url = `/${pathSnippets.slice(0, index + 1).join('/')}`.replace('//','/');
      return (
        <Breadcrumb.Item key={url}>
          <Link to={url}>{breadcrumbNameMap[url]}</Link>
        </Breadcrumb.Item>
      );
    });
    const selectedMenu = menuNameMap[pathSnippets];
    const breadcrumbItems = [
      <Breadcrumb.Item key="title">
        Lançamentos
      </Breadcrumb.Item>,
    ].concat(extraBreadcrumbItems);

    return (
      <Layout>
        <Header
          style={{
            position: 'fixed',
            zIndex: 1,
            width: '100%',
            height: '64px',
        }}>
          <div className="logo" />
          <Title className="pageTitle" level={2}>Lançamentos</Title>
        </Header>
        <Content style={{ padding: '0 50px', marginTop: 64 }}>
          <Breadcrumb style={{ margin: '16px 0', height: '24px' }}>
            {breadcrumbItems}
          </Breadcrumb>
          <Layout className="site-layout-background" style={{ minHeight: 'calc(100vh - 128px)' }}>
            <Sider
              className="site-layout-background"
              width={240}
              breakpoint="lg"
              collapsedWidth="0"
            >
              <Menu
                mode="inline"
                defaultSelectedKeys={[selectedMenu]}
                style={{ height: '100%' }}
              >
                <Menu.Item key="summary" icon={<ProfileOutlined />}>
                  <Link to="/">Resumo</Link>
                </Menu.Item>
                <Menu.Item key="nextLaunches" icon={<CalendarOutlined />}>
                  <Link to="/schedule">Próximos lançamentos</Link>
                </Menu.Item>
                <Menu.Item key="launchesHistory" icon={<HistoryOutlined />}>
                  <Link to="/history">Lançamentos anteriores</Link>
                </Menu.Item>
              </Menu>
            </Sider>
            <Content style={{ padding: '0 24px', minHeight: 280 }}>
              <Switch>
                <Route exact path="/" component={LaunchesSummary} />
                <Route exact path="/history" component={LaunchesHistory} />
                <Route exact path="/schedule" component={LaunchesSchedule} />
              </Switch>
            </Content>
          </Layout>
        </Content>
      </Layout>
    );
  }
}

const mapStateToProps = () => ({});

const mapDispatchToProps = (dispatch) => bindActionCreators(
  {
    changePage: (route) => push(route),
  },
  dispatch
);

const AppWithRouter = withRouter((props) => <App {...props} />);
export default connect(mapStateToProps, mapDispatchToProps)(AppWithRouter);
