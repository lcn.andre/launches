import React from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Timeline, Space } from 'antd';

import { getLaunchHistory } from '../../../modules/launches';

class LaunchesHistory extends React.Component {
  constructor(props) {
    super(props);
    this.getHistory = this.getHistory.bind(this);

    this.getHistory();
  }

  async getHistory() {
    await this.props.getLaunchHistory();
  }

  render() {
    let timeLineItems = [];

    if (this.props.launchHistory)
      timeLineItems = this.props.launchHistory.map((h, index) => (
        <Timeline.Item key={index} label={`${h.fireDate} - ${h.flightName}`}>{h.details}</Timeline.Item>
      ));

    return (
      <Space size="middle" align="start" style={{ width: '100%' }}>
        <Timeline
          mode="left"
          pending={this.props.launchHistoryLoading}
          style={{ margin: '32px auto', width: '60%', float: 'left' }}
        >
          {timeLineItems}
        </Timeline>
      </Space>
    )
  }
}

const mapStateToProps = ({ launches }) => ({
  launchHistory: launches.launchHistory,
  launchHistoryLoading: launches.launchHistoryLoading,
});

const mapDispatchToProps = (dispatch) => bindActionCreators(
  {
    getLaunchHistory,
  },
  dispatch
);

export default connect(mapStateToProps, mapDispatchToProps)(LaunchesHistory);
