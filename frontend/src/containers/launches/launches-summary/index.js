import React from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Space, Card, Steps } from 'antd';
import { Experiment, Variant } from 'react-optimize';

import { getSummary } from '../../../modules/launches';

const { Meta } = Card;

class LaunchesSummary extends React.Component {
  constructor(props) {
    super(props);
    this.getSummary = this.getSummary.bind(this);

    this.getSummary();
  }

  async getSummary() {
    await this.props.getSummary();
  }

  render() {
    return (
      <Space size="middle" direction="vertical" style={{ width: '100%' }}>
        <Experiment id="ar8zD-YdSUeZzdYkx-pXLQ">
          <Variant id="0">
            <Card title="Próximo lançamento" style={{ width: '100%' }} loading={this.props.summaryLoading}>
              {!this.props.summaryLoading && (<>
                <p><b>Data: </b>{ this.props.nextLaunch.fireDate }</p>
                <p><b>Nome: </b>{ this.props.nextLaunch.flightName }</p>

                <Meta description={ this.props.nextLaunch.details } />
              </>)}
            </Card>
            <Card title="Último lançamento" style={{ width: '100%' }} loading={this.props.summaryLoading}>
              {!this.props.summaryLoading && (<>
                <p><b>Data: </b>{ this.props.previousLaunch.fireDate }</p>
                <p><b>Nome: </b>{ this.props.previousLaunch.flightName }</p>

                <Meta description={ this.props.previousLaunch.details } />
              </>)}
            </Card>
          </Variant>
          <Variant id="1">
            {!this.props.summaryLoading && (<>
              <Steps current={1}>
                <Steps.Step title="Último lançamento" subTitle={ this.props.previousLaunch.fireDate } description={ this.props.previousLaunch.details } />
                <Steps.Step title="Próximo lançamento" subTitle={ this.props.nextLaunch.fireDate } description={ this.props.nextLaunch.details } />
              </Steps>
            </>)}
          </Variant>
        </Experiment>
      </Space>
    )
  }
}

const mapStateToProps = ({ launches }) => ({
  nextLaunch: launches.nextLaunch,
  previousLaunch: launches.previousLaunch,
  summaryLoading: launches.summaryLoading,
});

const mapDispatchToProps = (dispatch) => bindActionCreators(
  {
    getSummary,
  },
  dispatch
);

export default connect(mapStateToProps, mapDispatchToProps)(LaunchesSummary);
