import React from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Timeline, Space } from 'antd';

import { getLaunchSchedule } from '../../../modules/launches';

class LaunchesSchedule extends React.Component {
  constructor(props) {
    super(props);
    this.getSchedule = this.getSchedule.bind(this);

    this.getSchedule();
  }

  async getSchedule() {
    await this.props.getLaunchSchedule();
  }

  render() {
    let timeLineItems = [];

    if (this.props.launchSchedule)
      timeLineItems = this.props.launchSchedule.map((h, index) => (
        <Timeline.Item key={index} label={`${h.fireDate} - ${h.flightName}`}>{h.details}</Timeline.Item>
      ));

    return (
      <Space size="middle" align="start" style={{ width: '100%' }}>
        <Timeline
          mode="left"
          pending={this.props.launchScheduleLoading}
          style={{ margin: '32px auto', width: '60%', float: 'left' }}
        >
          {timeLineItems}
        </Timeline>
      </Space>
    )
  }
}

const mapStateToProps = ({ launches }) => ({
  launchSchedule: launches.launchSchedule,
  launchScheduleLoading: launches.launchScheduleLoading,
});

const mapDispatchToProps = (dispatch) => bindActionCreators(
  {
    getLaunchSchedule,
  },
  dispatch
);

export default connect(mapStateToProps, mapDispatchToProps)(LaunchesSchedule);
