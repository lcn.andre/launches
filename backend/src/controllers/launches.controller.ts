import { Controller, Get, InternalServerErrorException } from '@nestjs/common';
import { ApiCreatedResponse, ApiTags } from '@nestjs/swagger';

import { LaunchesService } from '../services/launches.service';
import { LaunchDto } from './dtos/launch.dto';

@ApiTags('Launches')
@Controller('launches')
export class LaunchesController {
    constructor(private readonly service: LaunchesService) {}

    @Get('/next')
    @ApiCreatedResponse({ type: LaunchDto })
    async getNextLaunch(): Promise<LaunchDto> {
        const result = await this.service.getNextLaunch();

        if (!result)
            throw new InternalServerErrorException('Ocorreu um erro ao buscar o próximo lançamento. Tente novamente mais tarde.');

        return result as LaunchDto;
    }

    @Get('/previous')
    @ApiCreatedResponse({ type: LaunchDto })
    async getPreviousLaunch(): Promise<LaunchDto> {
        const result = await this.service.getPreviousLaunch();

        if (!result)
            throw new InternalServerErrorException('Ocorreu um erro ao buscar o último lançamento. Tente novamente mais tarde.');

        return result as LaunchDto;
    }

    @Get('/history')
    @ApiCreatedResponse({ type: LaunchDto, isArray: true })
    async getLaunchHistory(): Promise<LaunchDto[]> {
        const result = await this.service.getLaunchHistory();

        if (!result)
            throw new InternalServerErrorException('Ocorreu um erro ao buscar o histórico de lançamentos. Tente novamente mais tarde.');

        return result.map(h => (h as LaunchDto));
    }

    @Get('/schedule')
    @ApiCreatedResponse({ type: LaunchDto, isArray: true })
    async getLaunchSchedule(): Promise<LaunchDto[]> {
        const result = await this.service.getLaunchSchedule();

        if (!result)
            throw new InternalServerErrorException('Ocorreu um erro ao buscar os próximos lançamentos. Tente novamente mais tarde.');

        return result.map(h => (h as LaunchDto));
    }
}