import { CqrsModule } from '@nestjs/cqrs';
import { Test } from '@nestjs/testing';

import { SpaceXProxy } from '../integrations/spacex.proxy';
import { LaunchesService } from '../services/launches.service';
import { LaunchesController } from './launches.controller';
import { GetNextLaunchHandler, GetNextLaunchQuery } from '../use-cases/get-next-launch';
import { GetLaunchHistoryHandler, GetLaunchHistoryQuery } from '../use-cases/get-launch-history';
import { GetLaunchScheduleHandler, GetLaunchScheduleQuery } from '../use-cases/get-launch-schedule';
import { GetPreviousLaunchQuery, GetPreviousLaunchHandler } from '../use-cases/get-previous-launch';

describe('[Controller] launches', () => {
    let controller: LaunchesController;

    beforeEach(async () => {
        const module = await Test.createTestingModule({
            imports: [CqrsModule],
            controllers: [LaunchesController],
            providers: [
                SpaceXProxy,
                LaunchesService,
                GetNextLaunchQuery,
                GetNextLaunchHandler,
                GetPreviousLaunchQuery,
                GetPreviousLaunchHandler,
                GetLaunchHistoryQuery,
                GetLaunchHistoryHandler,
                GetLaunchScheduleQuery,
                GetLaunchScheduleHandler,
            ]
        }).compile();

        controller = module.get<LaunchesController>(LaunchesController);
        await module.createNestApplication().init();
    });

    it ('should return the next launch', async () => {
        const result = await controller.getNextLaunch();

        expect(result).toBeDefined();
        expect(result.fireDate).toBeInstanceOf(Date);
    });

    it ('should return the previous launch', async () => {
        const result = await controller.getPreviousLaunch();

        expect(result).toBeDefined();
        expect(result.fireDate).toBeInstanceOf(Date);
    });

    it ('should return the launch history', async () => {
        const result = await controller.getLaunchHistory();

        expect(result).toBeDefined();
        expect(result.length).toBeDefined();
        expect(result.length).toBeGreaterThan(0);
        expect(result[0].fireDate).toBeInstanceOf(Date);
    });

    it ('should return the launch schedule', async () => {
        const result = await controller.getLaunchSchedule();

        expect(result).toBeDefined();
        expect(result.length).toBeDefined();
        expect(result.length).toBeGreaterThan(0);
        expect(result[0].fireDate).toBeInstanceOf(Date);
    });
});
