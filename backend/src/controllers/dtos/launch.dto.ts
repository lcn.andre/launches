import { ApiProperty } from '@nestjs/swagger';

export class LaunchDto {
    @ApiProperty()
    flightName: string;
    @ApiProperty()
    fireDate: Date;
    @ApiProperty()
    details: string;
}
