import * as spacex from 'spacex-api.js';
import { Injectable, Logger } from '@nestjs/common';

import { Launch } from '../entities/launch.entity';

@Injectable()
export class SpaceXProxy {
  private readonly logger = new Logger(SpaceXProxy.name);

  public getPreviousLaunch(): Promise<Launch> {
    return this.fetchLaunches('latest') as Promise<Launch>;
  }

  public getNextLaunch(): Promise<Launch> {
    return this.fetchLaunches('next') as Promise<Launch>;
  }

  public getLaunchHistory(): Promise<Launch[]> {
    return this.fetchLaunches('past') as Promise<Launch[]>;
  }

  public getLaunchSchedule(): Promise<Launch[]> {
    return this.fetchLaunches('upcoming') as Promise<Launch[]>;
  }

  private async fetchLaunches(filter: string): Promise<Launch | Launch[]> {
    try {
        const launchData = await spacex.getData('launches', filter);

        return launchData.length
               ? launchData.slice(0, 5).map((h: any) => this.toEntity(h))
               : this.toEntity(launchData);
      } catch (err) {
        this.logger.error(err.message, err.stack);
        return null;
      }
  }

  private toEntity(flight: any): Launch {
    const launch = new Launch();

    launch.details = flight.details;
    launch.fireDate = new Date(Date.parse(flight.date_utc));
    launch.flightName = flight.name;

    return launch;
  }
}
