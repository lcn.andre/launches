import { QueryHandler, IQueryHandler } from '@nestjs/cqrs';
import { Injectable } from '@nestjs/common';

import { SpaceXProxy } from '../integrations/spacex.proxy';
import { Launch } from '../entities/launch.entity';

export class GetLaunchHistoryQuery { }

@Injectable()
@QueryHandler(GetLaunchHistoryQuery)
export class GetLaunchHistoryHandler implements IQueryHandler<GetLaunchHistoryQuery> {
    constructor(private readonly proxy: SpaceXProxy) {}

    async execute(query: GetLaunchHistoryQuery): Promise<Launch[]> {
        return this.proxy.getLaunchHistory();
    }
}
