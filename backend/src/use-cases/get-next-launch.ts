import { QueryHandler, IQueryHandler } from '@nestjs/cqrs';
import { Injectable } from '@nestjs/common';

import { SpaceXProxy } from '../integrations/spacex.proxy';
import { Launch } from '../entities/launch.entity';

export class GetNextLaunchQuery { }

@Injectable()
@QueryHandler(GetNextLaunchQuery)
export class GetNextLaunchHandler implements IQueryHandler<GetNextLaunchQuery> {
    constructor(private readonly proxy: SpaceXProxy) {}

    async execute(query: GetNextLaunchQuery): Promise<Launch> {
        return this.proxy.getNextLaunch();
    }
}
