import { QueryHandler, IQueryHandler } from '@nestjs/cqrs';
import { Injectable } from '@nestjs/common';

import { SpaceXProxy } from '../integrations/spacex.proxy';
import { Launch } from '../entities/launch.entity';

export class GetPreviousLaunchQuery { }

@Injectable()
@QueryHandler(GetPreviousLaunchQuery)
export class GetPreviousLaunchHandler implements IQueryHandler<GetPreviousLaunchQuery> {
    constructor(private readonly proxy: SpaceXProxy) {}

    async execute(query: GetPreviousLaunchQuery): Promise<Launch> {
        return this.proxy.getPreviousLaunch();
    }
}
