import { QueryHandler, IQueryHandler } from '@nestjs/cqrs';
import { Injectable } from '@nestjs/common';

import { SpaceXProxy } from '../integrations/spacex.proxy';
import { Launch } from '../entities/launch.entity';

export class GetLaunchScheduleQuery { }

@Injectable()
@QueryHandler(GetLaunchScheduleQuery)
export class GetLaunchScheduleHandler implements IQueryHandler<GetLaunchScheduleQuery> {
    constructor(private readonly proxy: SpaceXProxy) {}

    async execute(query: GetLaunchScheduleQuery): Promise<Launch[]> {
        return this.proxy.getLaunchSchedule();
    }
}
