import { Test } from '@nestjs/testing';

import { GetNextLaunchQuery, GetNextLaunchHandler } from './get-next-launch';
import { SpaceXProxy } from '../integrations/spacex.proxy';

describe('[Use case] get-next-launch', () => {
  let proxy: SpaceXProxy;

  beforeEach(async () => {
    const module = await Test.createTestingModule({
      providers: [SpaceXProxy],
    }).compile();

    proxy = module.get<SpaceXProxy>(SpaceXProxy);
  });

  it('should return the next launch', async () => {
    const query = new GetNextLaunchQuery();
    const handler = new GetNextLaunchHandler(proxy);
    const result = await handler.execute(query);

    expect(result).toBeDefined();
    expect(result.fireDate).toBeInstanceOf(Date);
  });
});
