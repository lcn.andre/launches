import { Test } from '@nestjs/testing';

import { GetPreviousLaunchQuery, GetPreviousLaunchHandler } from './get-previous-launch';
import { SpaceXProxy } from '../integrations/spacex.proxy';

describe('[Use case] get-previous-launch', () => {
  let proxy: SpaceXProxy;

  beforeEach(async () => {
    const module = await Test.createTestingModule({
      providers: [SpaceXProxy],
    }).compile();

    proxy = module.get<SpaceXProxy>(SpaceXProxy);
  });

  it('should return the previous launch', async () => {
    const query = new GetPreviousLaunchQuery();
    const handler = new GetPreviousLaunchHandler(proxy);
    const result = await handler.execute(query);

    expect(result).toBeDefined();
    expect(result.fireDate).toBeInstanceOf(Date);
  });
});
