import { Test } from '@nestjs/testing';

import { GetLaunchScheduleQuery, GetLaunchScheduleHandler } from './get-launch-schedule';
import { SpaceXProxy } from '../integrations/spacex.proxy';

describe('[Use case] get-launch-schedule', () => {
  let proxy: SpaceXProxy;

  beforeEach(async () => {
    const module = await Test.createTestingModule({
      providers: [SpaceXProxy],
    }).compile();

    proxy = module.get<SpaceXProxy>(SpaceXProxy);
  });

  it('should return launch schedule', async () => {
    const query = new GetLaunchScheduleQuery();
    const handler = new GetLaunchScheduleHandler(proxy);
    const result = await handler.execute(query);

    expect(result).toBeDefined();
    expect(result.length).toBeDefined();
    expect(result.length).toBeGreaterThan(0);
    expect(result[0].fireDate).toBeInstanceOf(Date);
  });
});
