import { Test } from '@nestjs/testing';

import { GetLaunchHistoryQuery, GetLaunchHistoryHandler } from './get-launch-history';
import { SpaceXProxy } from '../integrations/spacex.proxy';

describe('[Use case] get-launch-history', () => {
  let proxy: SpaceXProxy;

  beforeEach(async () => {
    const module = await Test.createTestingModule({
      providers: [SpaceXProxy],
    }).compile();

    proxy = module.get<SpaceXProxy>(SpaceXProxy);
  });

  it('should return launch history', async () => {
    const query = new GetLaunchHistoryQuery();
    const handler = new GetLaunchHistoryHandler(proxy);
    const result = await handler.execute(query);

    expect(result).toBeDefined();
    expect(result.length).toBeDefined();
    expect(result.length).toBeGreaterThan(0);
    expect(result[0].fireDate).toBeInstanceOf(Date);
  });
});
