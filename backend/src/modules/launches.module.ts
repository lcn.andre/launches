/* istanbul ignore file */
import { Module } from '@nestjs/common';
import { CqrsModule } from '@nestjs/cqrs';

import { LaunchesController } from '../controllers/launches.controller';
import { LaunchesService } from '../services/launches.service';
import { SpaceXProxy } from '../integrations/spacex.proxy';
import { GetNextLaunchHandler, GetNextLaunchQuery } from '../use-cases/get-next-launch';
import { GetPreviousLaunchHandler, GetPreviousLaunchQuery } from '../use-cases/get-previous-launch';
import { GetLaunchHistoryHandler, GetLaunchHistoryQuery } from '../use-cases/get-launch-history';
import { GetLaunchScheduleHandler, GetLaunchScheduleQuery } from '../use-cases/get-launch-schedule';

@Module({
  imports: [CqrsModule],
  providers: [
    SpaceXProxy,
    LaunchesService,
    GetNextLaunchQuery,
    GetNextLaunchHandler,
    GetPreviousLaunchQuery,
    GetPreviousLaunchHandler,
    GetLaunchHistoryQuery,
    GetLaunchHistoryHandler,
    GetLaunchScheduleQuery,
    GetLaunchScheduleHandler,
  ],
  controllers: [LaunchesController],
  exports: [CqrsModule],
})
export class LaunchesModule {}
