/* istanbul ignore file */
import { Module } from '@nestjs/common';
import { AppController } from '../controllers/app.controller';
import { LaunchesModule } from './launches.module';

@Module({
  imports: [LaunchesModule],
  controllers: [AppController],
})
export class AppModule {}
