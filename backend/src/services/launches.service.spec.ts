import { CqrsModule } from '@nestjs/cqrs';
import { Test } from '@nestjs/testing';

import { SpaceXProxy } from '../integrations/spacex.proxy';
import { LaunchesService } from './launches.service';
import { GetNextLaunchHandler, GetNextLaunchQuery } from '../use-cases/get-next-launch';
import { GetPreviousLaunchHandler, GetPreviousLaunchQuery } from '../use-cases/get-previous-launch';
import { GetLaunchHistoryHandler, GetLaunchHistoryQuery } from '../use-cases/get-launch-history';
import { GetLaunchScheduleHandler, GetLaunchScheduleQuery } from '../use-cases/get-launch-schedule';

describe('[Service] launches', () => {
    let service: LaunchesService;

    beforeEach(async () => {
        const module = await Test.createTestingModule({
            imports: [CqrsModule],
            providers: [
                SpaceXProxy,
                LaunchesService,
                GetNextLaunchQuery,
                GetNextLaunchHandler,
                GetPreviousLaunchQuery,
                GetPreviousLaunchHandler,
                GetLaunchHistoryQuery,
                GetLaunchHistoryHandler,
                GetLaunchScheduleQuery,
                GetLaunchScheduleHandler,
            ]
        }).compile();

        service = module.get<LaunchesService>(LaunchesService);
        await module.createNestApplication().init();
    });

    it ('should return the next launch', async () => {
        const result = await service.getNextLaunch();

        expect(result).toBeDefined();
        expect(result.fireDate).toBeInstanceOf(Date);
    });

    it ('should return the previous launch', async () => {
        const result = await service.getPreviousLaunch();

        expect(result).toBeDefined();
        expect(result.fireDate).toBeInstanceOf(Date);
    });

    it ('should return the launch history', async () => {
        const result = await service.getLaunchHistory();

        expect(result).toBeDefined();
        expect(result.length).toBeDefined();
        expect(result.length).toBeGreaterThan(0);
        expect(result[0].fireDate).toBeInstanceOf(Date);
    });

    it ('should return the launch schedule', async () => {
        const result = await service.getLaunchSchedule();

        expect(result).toBeDefined();
        expect(result.length).toBeDefined();
        expect(result.length).toBeGreaterThan(0);
        expect(result[0].fireDate).toBeInstanceOf(Date);
    });
});