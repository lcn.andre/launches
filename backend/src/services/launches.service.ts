import { Injectable } from '@nestjs/common';
import { QueryBus } from '@nestjs/cqrs';

import { Launch } from '../entities/launch.entity';
import { GetNextLaunchQuery } from '../use-cases/get-next-launch';
import { GetPreviousLaunchQuery } from '../use-cases/get-previous-launch';
import { GetLaunchHistoryQuery } from '../use-cases/get-launch-history';
import { GetLaunchScheduleQuery } from '../use-cases/get-launch-schedule';

@Injectable()
export class LaunchesService {
    constructor(private queryBus: QueryBus) { }

    getNextLaunch(): Promise<Launch> {
        return this.queryBus.execute(
            new GetNextLaunchQuery()
        );
    }

    getPreviousLaunch(): Promise<Launch> {
        return this.queryBus.execute(
            new GetPreviousLaunchQuery()
        );
    }

    getLaunchHistory(): Promise<Launch[]> {
        return this.queryBus.execute(
            new GetLaunchHistoryQuery()
        );
    }

    getLaunchSchedule(): Promise<Launch[]> {
        return this.queryBus.execute(
            new GetLaunchScheduleQuery()
        );
    }
}
