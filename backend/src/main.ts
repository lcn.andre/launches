/* istanbul ignore file */
import { NestFactory } from '@nestjs/core';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';

import { AppModule } from './modules/app.module';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);

  const config = new DocumentBuilder()
    .setTitle('Launches API')
    .setDescription('API for Launches App')
    .setVersion('1.0')
    .addTag('Launches')
    .build();

  const document = SwaggerModule.createDocument(app, config);
  SwaggerModule.setup('api', app, document);

  app.enableCors({
    origin: true,
    allowedHeaders: 'Accept,Accept-Encoding,Accept-Language,Connection,Content-Length,Content-Type,Host,Origin,Referer,Sec-Fetch-Dest,Sec-Fetch-Mode,Sec-Fetch-Site,User-Agent,Authorization',
    methods: 'GET,PATCH,POST,DELETE,OPTIONS',
    credentials: true,
    exposedHeaders: '*',
    optionsSuccessStatus: 204,
  });

  await app.listen(process.env.PORT || 5000, '0.0.0.0');
}

bootstrap();
